<?php

require_once 'vendor/autoload.php';

$transporter = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
  ->setUsername('$this->username')
  ->setPassword('$this->password');

$mailer = Swift_Mailer::newInstance($transporter);

// Create a message
$message = (new Swift_Message('Wonderful Subject'))
  ->setFrom(['john@blah.com' => 'John Blah'])
  ->setTo(['receiver@domain.org', 'other@domain.org' => 'A name'])
  ->setBody('Here is the message walah')
  ;

// Send the message
$result = $mailer->send($message);